function checkPass() {
    // set password variable
    var pswd = $('#fos_user_registration_form_plainPassword_first').val();
    if (pswd) {
        var lengthOK=false, letterOk=false, capitalOk=false, numberOk=false;

        //validate the length
        if (pswd.length < 8) {
            $('#length').removeClass('valid').addClass('invalid');
        } else {
            $('#length').removeClass('invalid').addClass('valid');
            lengthOK = true;
        }

        //validate letter
        if (pswd.match(/[A-z]/)) {
            $('#letter').removeClass('invalid').addClass('valid');
            letterOk = true;
        } else {
            $('#letter').removeClass('valid').addClass('invalid');
        }

        //validate uppercase letter
        if (pswd.match(/[A-Z]/)) {
            $('#capital').removeClass('invalid').addClass('valid');
            capitalOk = true;
        } else {
            $('#capital').removeClass('valid').addClass('invalid');
        }

        //validate number
        if (pswd.match(/\d/)) {
            $('#number').removeClass('invalid').addClass('valid');
            numberOk = true;
        } else {
            $('#number').removeClass('valid').addClass('invalid');
        }

        if (numberOk && letterOk && capitalOk && lengthOK) {
            $('#pswd_info').css('display', 'none');
            return true;
        }
    }
    return false;
}

function checkEmail() {
    $.ajax({
        type: 'POST',
        url: Routing.generate('ajax_check_email'),
        dataType: 'json',
        data: {email: $('#fos_user_registration_form_email').val()},
        async: true
    })
        .then(function (response) {
            if (!response.result) { $('#emailError').show(); $('a[href="#finish"]').attr('href', '#shift'); }
            else {  $('#emailError').hide(); $('a[href="#shift"]').attr('href', '#finish'); }
        })
        .catch(function (reason) {
            // console.log(reason);
    });
}

$(document).ready(function() {
    // you have to use keyup, because keydown will not catch the currently entered value
    $('#fos_user_registration_form_plainPassword_first').keyup(checkPass)
    .focus(function() {
        $('#pswd_info').show();
    })
    .blur(function() {
        $('#pswd_info').hide();
    });

    $('#fos_user_registration_form_email').keyup(checkEmail)
});
-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le :  jeu. 14 nov. 2019 à 13:40
-- Version du serveur :  10.3.20-MariaDB
-- Version de PHP :  7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `cacket_icar_api`
--

-- --------------------------------------------------------

--
-- Structure de la table `booking`
--

CREATE TABLE `booking` (
  `id` int(11) NOT NULL,
  `travel_id` int(11) NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `cust_first_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cust_last_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cust_tel` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cust_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `confirmed_at` datetime DEFAULT NULL,
  `amount_paid` int(11) DEFAULT NULL,
  `payment` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_ref` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `booking`
--

INSERT INTO `booking` (`id`, `travel_id`, `status`, `cust_first_name`, `cust_last_name`, `cust_tel`, `cust_address`, `code`, `created_at`, `confirmed_at`, `amount_paid`, `payment`, `payment_ref`) VALUES
(3, 2, 1, 'Abalo', 'Komla', '99432213', 'Lomé', 'A5258', '2019-09-16 13:46:43', '2019-09-16 13:46:43', 5000, 'FLOOZ', NULL),
(4, 2, 0, 'Kodjo', 'Kouma', '92805323', 'Lomé', 'A52S8', '2019-09-16 13:46:43', '2019-09-16 13:46:43', 5000, 'TMONEY', NULL),
(5, 2, 0, 'Paul', 'Alognon-Anani', '92805323', 'Kegue', NULL, '2019-10-01 18:25:20', NULL, NULL, NULL, NULL),
(9, 2, 0, 'Paul', 'Alognon-Anani', '92805323', 'Kegue', NULL, '2019-10-01 18:37:52', NULL, NULL, NULL, NULL),
(10, 2, 1, 'Paul', 'Alognon-Anani', '92805323', 'Kegue', NULL, '2019-10-01 18:39:23', '2019-10-01 18:39:25', 2, 'flooz', NULL),
(11, 2, 0, 'Paul', 'Alognon-Anani', '92805323', 'Kegue', NULL, '2019-10-01 18:44:11', NULL, NULL, NULL, NULL),
(12, 2, 0, 'Paul', 'Alognon-Anani', '92805323', 'Kegue', NULL, '2019-10-01 18:45:58', NULL, NULL, NULL, NULL),
(13, 2, 0, 'Paul', 'Alognon-Anani', '92805323', 'Kegue', NULL, '2019-10-08 14:29:56', NULL, NULL, NULL, NULL),
(14, 2, 0, 'Paul', 'Alognon-Anani', '92805323', 'Kegue', NULL, '2019-10-08 14:33:38', NULL, NULL, NULL, NULL),
(15, 2, 0, 'Universite', 'Lome', '92805323', 'Kegue', NULL, '2019-10-08 14:36:31', NULL, NULL, NULL, NULL),
(16, 2, 0, 'Universite', 'Lome', '92805323', 'Kegue', NULL, '2019-10-08 14:52:02', NULL, NULL, NULL, NULL),
(17, 2, 0, 'Universite', 'Lome', '92805323', 'Kegue', NULL, '2019-10-08 14:52:53', NULL, NULL, NULL, NULL),
(18, 2, 0, 'Paul', 'Alognon-Anani', '92805323', 'Kegue', NULL, '2019-10-08 14:57:50', NULL, NULL, NULL, NULL),
(19, 2, 0, 'Paul', 'Alognon-Anani', '92805323', 'Kegue', NULL, '2019-10-08 15:01:27', NULL, NULL, NULL, NULL),
(20, 2, 0, 'Paul', 'Alognon-Anani', '92805323', 'Kegue', NULL, '2019-10-08 15:14:29', NULL, NULL, NULL, NULL),
(21, 2, 0, 'Paul', 'Alognon-Anani', '92805323', 'Kegue', NULL, '2019-10-08 15:18:55', NULL, NULL, NULL, NULL),
(22, 2, 0, 'Paul', 'Alognon-Anani', '92805323', 'Kegue', NULL, '2019-10-08 16:15:45', NULL, NULL, NULL, NULL),
(23, 2, 0, 'Paul', 'Alognon-Anani', '92805323', 'Kegue', NULL, '2019-10-08 16:22:43', NULL, NULL, NULL, NULL),
(24, 2, 0, 'Paul', 'Alognon-Anani', '92805323', 'Kegue', NULL, '2019-10-08 16:59:42', NULL, NULL, NULL, NULL),
(25, 2, 0, 'Paul', 'Alognon-Anani', '92805323', 'Kergue', NULL, '2019-10-08 17:07:02', NULL, NULL, NULL, NULL),
(26, 2, 1, 'Paul', 'Alognon-Anani', '92805323', 'Kepe', NULL, '2019-10-08 17:13:16', '2019-10-08 17:13:21', NULL, NULL, NULL),
(27, 2, 1, 'Paul', 'Alognon-Anani', '92805323', 'Kegue', NULL, '2019-10-08 17:17:22', '2019-10-08 17:17:26', NULL, NULL, NULL),
(28, 2, 0, 'Paul', 'Alognon-Anani', '92805323', 'Kegue', NULL, '2019-10-08 17:19:40', NULL, NULL, NULL, NULL),
(29, 2, 0, 'Paul', 'Alognon-Anani', '92805323', 'Kegue', NULL, '2019-10-08 17:20:45', NULL, NULL, NULL, NULL),
(30, 2, 0, 'Paul', 'Alognon-Anani', '92805323', 'Kegue', NULL, '2019-10-08 17:23:01', NULL, NULL, NULL, NULL),
(31, 2, 0, 'Paul', 'Alognon-Anani', '92805323', 'Kegue', NULL, '2019-10-08 17:23:54', NULL, NULL, NULL, NULL),
(32, 2, 0, 'Paul', 'Alognon-Anani', '92805323', 'Kegue', NULL, '2019-10-08 17:24:55', NULL, NULL, NULL, NULL),
(33, 2, 0, 'Paul', 'Alognon-Anani', '92805323', 'Kegue', NULL, '2019-10-08 17:26:28', NULL, NULL, NULL, NULL),
(34, 2, 0, 'Paul', 'Alognon-Anani', '92805323', 'Kegue', NULL, '2019-10-08 17:28:29', NULL, NULL, NULL, NULL),
(35, 2, 0, 'Paul', 'Alognon-Anani', '92805323', 'g', NULL, '2019-10-08 17:33:15', NULL, NULL, NULL, NULL),
(36, 2, 0, 'Paul', 'Alognon-Anani', '92805323', 'ku', NULL, '2019-10-08 17:34:46', NULL, NULL, NULL, NULL),
(37, 2, 0, 'Paul', 'Alognon-Anani', '92805323', 'kegue', NULL, '2019-10-08 17:39:05', NULL, NULL, NULL, NULL),
(38, 2, 0, 'Paul', 'Alognon-Anani', '92805323', 'ju', NULL, '2019-10-08 17:41:41', NULL, NULL, NULL, NULL),
(39, 2, 0, 'Paul', 'Alognon-Anani', '92805323', 'ouip', NULL, '2019-10-08 17:44:01', NULL, NULL, NULL, NULL),
(40, 2, 0, 'Paul', 'Alognon-Anani', '92805323', 'POI', NULL, '2019-10-08 17:47:21', NULL, NULL, NULL, NULL),
(41, 2, 0, 'Paul', 'Alognon-Anani', '92805323', 'fD', NULL, '2019-10-08 17:48:50', NULL, NULL, 'FLOOZ', '38079'),
(42, 2, 0, 'Paul', 'Alognon-Anani', '92805323', 'Dede', NULL, '2019-10-09 09:18:54', NULL, NULL, NULL, NULL),
(43, 2, 0, 'Paul', 'Alognon-Anani', '92805323', 'D', NULL, '2019-10-09 09:24:23', NULL, NULL, NULL, NULL),
(44, 2, 0, 'Paul', 'Alognon-Anani', '92805323', 'DE', NULL, '2019-10-09 09:43:13', NULL, NULL, NULL, NULL),
(45, 2, 1, 'Paul', 'Alognon-Anani', '92805323', 'fe', 'EPXGDE', '2019-10-09 10:12:46', '2019-10-09 10:12:51', NULL, 'FLOOZ', '38130'),
(46, 2, 0, 'Ossi', 'Loi', '99997684', 'Zongo', NULL, '2019-10-18 17:47:50', NULL, NULL, NULL, NULL),
(47, 2, 1, 'Kokou', 'Logli', '99997684', 'Kegue\n', 'Y7NM@1', '2019-10-23 10:07:19', '2019-10-23 10:07:29', NULL, 'FLOOZ', '40374'),
(48, 2, 1, 'Kao', 'Liglo', '99997684', 'FKF', 'KG9UOM', '2019-10-23 10:29:37', '2019-10-23 10:29:45', NULL, 'FLOOZ', '40379'),
(49, 2, 1, 'Kilo', 'LOLO', '99997684', 'JDR', 'VCZWXU', '2019-10-23 10:37:13', '2019-10-23 10:37:19', NULL, 'FLOOZ', '40381'),
(50, 2, 1, 'Dekon', 'LOGLI', '99997684', 'Kegue\n', 'BE4PIX', '2019-10-23 12:36:48', '2019-10-23 12:37:00', NULL, 'FLOOZ', '40402'),
(51, 2, 1, 'Kegue', 'Kegue', '99997684', 'Kegue', 'UMML8B', '2019-10-23 12:40:27', '2019-10-23 12:40:35', NULL, 'FLOOZ', '40403'),
(52, 2, 1, 'Universite', 'Lome', '99997684', 'asas', 'OF2BQS', '2019-10-23 13:57:56', '2019-10-23 13:58:06', NULL, 'FLOOZ', '40415'),
(53, 2, 1, 'LOl', 'Legal', '99997684', 'Kegue\n', 'HH$CR9', '2019-10-23 15:04:37', '2019-10-23 15:04:47', NULL, 'FLOOZ', '40428'),
(54, 2, 1, 'Universite', 'Lome', '99997684', 'Kegue', 'V2$65U', '2019-10-23 15:09:45', '2019-10-23 15:09:54', NULL, 'FLOOZ', '40431'),
(55, 2, 1, 'Universite', 'Lome', '99997684', 'DE', 'YZYJHC', '2019-10-23 15:14:16', '2019-10-23 15:14:22', NULL, 'FLOOZ', '40434'),
(56, 2, 1, 'Paul', 'Alognon-Anani', '92805323', 'DED', 'QT#QPX', '2019-10-23 16:40:28', '2019-10-23 16:40:37', NULL, 'FLOOZ', '40448'),
(57, 2, 0, 'Paul', 'Alognon-Anani', '92805323', 'SW', NULL, '2019-10-23 16:46:04', NULL, NULL, 'FLOOZ', '40450'),
(58, 2, 1, 'Paul', 'Alognon-Anani', '92805323', ':OK', 'LMJ@NM', '2019-10-23 17:39:50', '2019-10-23 17:39:58', NULL, 'FLOOZ', '40458'),
(59, 2, 1, 'Paul', 'Alognon-Anani', '92805323', 'LO', 'M63N3M', '2019-10-23 17:42:52', '2019-10-23 17:43:13', NULL, 'FLOOZ', '40460'),
(60, 2, 1, 'Lome', 'Universite', '99997684', ':Ls', 'NO5TIX', '2019-10-29 08:35:38', '2019-10-29 08:35:55', NULL, 'FLOOZ', '41083'),
(61, 2, 1, 'DES', 'SWD', '99997684', 'DED', 'YUQE7E', '2019-10-29 08:39:33', '2019-10-29 08:39:41', NULL, 'FLOOZ', '41084'),
(68, 2, 0, 'Paul', 'Alognon-Anani', '92805323', 'DE', NULL, '2019-10-29 09:35:37', NULL, NULL, NULL, NULL),
(69, 2, 0, 'Paul', 'Alognon-Anani', '92805323', 'DEE', NULL, '2019-10-29 09:37:33', NULL, NULL, NULL, NULL),
(70, 2, 0, 'adkad', 'Paol', '92805323', 'DES', NULL, '2019-10-30 10:59:46', NULL, NULL, NULL, NULL),
(71, 2, 0, 'Kogbe', 'Edem', '99997684', 'Des', NULL, '2019-10-30 11:24:55', NULL, NULL, NULL, NULL),
(72, 2, 0, 'De', 'Dele', '99997684', 'Dw', NULL, '2019-10-30 11:28:51', NULL, NULL, NULL, NULL),
(73, 2, 0, 'swd', 'DE', '99997684', 'dDE', NULL, '2019-10-30 11:53:59', NULL, NULL, NULL, NULL),
(74, 2, 0, 'DEs', 'DSE', '99997684', 'dde', NULL, '2019-10-30 11:59:11', NULL, NULL, NULL, NULL),
(75, 2, 0, 'A', 'Q', '99997684', 'D', NULL, '2019-10-30 17:59:56', NULL, NULL, NULL, NULL),
(76, 2, 0, 'A', 'Q', '99997684', 'D', NULL, '2019-10-30 18:00:13', NULL, NULL, NULL, NULL),
(77, 2, 0, 'A', 'Q', '99997684', 'D', NULL, '2019-10-30 18:00:28', NULL, NULL, NULL, NULL),
(79, 2, 0, 'A', 'Q', '92805323', 'D', NULL, '2019-10-30 18:01:04', NULL, NULL, NULL, NULL),
(80, 2, 0, 'KO', 'LP', '92805323', 'SW', NULL, '2019-10-30 18:12:12', NULL, NULL, NULL, NULL),
(81, 2, 0, 'SS', 'A', '92805323', 'SS', NULL, '2019-10-30 18:36:40', NULL, NULL, NULL, NULL),
(82, 2, 0, 'SA', 'A', '96028928', 'SA', NULL, '2019-10-30 18:39:20', NULL, NULL, NULL, NULL),
(83, 2, 0, 'SS', 'A', '96028928', 'DS', NULL, '2019-10-30 18:40:57', NULL, NULL, NULL, NULL),
(84, 2, 0, 'S', 'A', '96028928', 'SDS', NULL, '2019-10-30 18:41:54', NULL, NULL, NULL, NULL),
(85, 2, 0, 'dse', 'ASQDDq', '96028928', 'de', NULL, '2019-10-30 18:46:30', NULL, NULL, NULL, NULL),
(86, 2, 0, 'SAS', 'SQ', '96028928', 'SDD', NULL, '2019-10-30 18:48:02', NULL, NULL, NULL, NULL),
(87, 2, 1, 'DEDE', 'SDD', '96028928', 'DE', 'T$11XD', '2019-10-30 18:54:24', '2019-10-30 18:54:32', NULL, 'FLOOZ', '41254'),
(88, 2, 0, 'ASW', 'ASA', '96028928', 'ghg', NULL, '2019-10-30 19:07:59', NULL, NULL, NULL, NULL),
(89, 2, 0, 'ASW', 'ASA', '96028928', 'ghg', NULL, '2019-10-30 19:08:11', NULL, NULL, NULL, NULL),
(90, 2, 0, 'ASSA', 'AS', '96028928', 'SDS', NULL, '2019-10-30 19:16:21', NULL, NULL, NULL, NULL),
(91, 2, 0, 'ASSA', 'AS', '96028928', 'SDS', NULL, '2019-10-30 19:16:31', NULL, NULL, NULL, NULL),
(92, 2, 0, 'SA', 'QW', '96028928', 'SDD', NULL, '2019-10-31 09:39:47', NULL, NULL, NULL, NULL),
(93, 2, 1, 'KOs', 'ASL', '96028928', 'DSD', 'ZK2ZKE', '2019-10-31 09:56:08', '2019-10-31 09:56:16', NULL, 'FLOOZ', '41309'),
(94, 2, 1, 'SD', 'SD', '96028928', 'DED', 'K#BEZ6', '2019-10-31 09:58:04', '2019-10-31 09:58:11', NULL, 'FLOOZ', '41310'),
(95, 2, 0, 'h', ':hyg', '96028928', 'Dede', NULL, '2019-10-31 10:52:56', NULL, NULL, NULL, NULL),
(96, 2, 0, 'h', ':hyg', '96028928', 'Dede', NULL, '2019-10-31 10:53:11', NULL, NULL, NULL, NULL),
(97, 2, 0, 'h', ':hyg', '96028928', 'Dede', NULL, '2019-10-31 10:53:33', NULL, NULL, NULL, NULL),
(98, 2, 0, 'h', ':hyg', '96028928', 'Dede', NULL, '2019-10-31 10:53:56', NULL, NULL, NULL, NULL),
(99, 2, 0, 'h', ':hyg', '96028928', 'Dede', NULL, '2019-10-31 10:55:01', NULL, NULL, NULL, NULL),
(100, 2, 0, 'h', ':hyg', '96028928', 'Dede', NULL, '2019-10-31 10:55:15', NULL, NULL, NULL, NULL),
(101, 2, 0, 'h', ':hyg', '96028928', 'Dede', NULL, '2019-10-31 10:55:58', NULL, NULL, NULL, NULL),
(102, 2, 0, 'Azerty', 'Qwerty', '96028928', 'Sedr', NULL, '2019-10-31 12:35:09', NULL, NULL, NULL, NULL),
(103, 2, 1, 'Rd', 'Ft', '96028928', 'Gd', 'SET4R3', '2019-10-31 12:58:00', '2019-10-31 12:58:11', NULL, 'FLOOZ', '41334'),
(104, 2, 1, 'Qwerty', 'Quty', '96028928', 'De', 'WLBF&Z', '2019-10-31 13:46:41', '2019-10-31 13:46:50', NULL, 'FLOOZ', '41342'),
(105, 2, 1, 'Qwerty', 'Quty', '96028928', 'De', 'UY6AGU', '2019-10-31 13:52:12', '2019-10-31 13:52:19', NULL, 'FLOOZ', '41345'),
(106, 2, 1, 'Azerty', 'Qwerty', '96028928', 'Qsx', 'KKE9P9', '2019-10-31 14:05:57', '2019-10-31 14:06:07', NULL, 'FLOOZ', '41354'),
(107, 2, 0, ':O:', 'A', '96028928', 'P:OL', NULL, '2019-10-31 14:21:32', NULL, NULL, NULL, NULL),
(108, 2, 0, 'O', 'A', '96028928', 'POL', NULL, '2019-10-31 14:31:00', NULL, NULL, NULL, NULL),
(109, 2, 1, '95', '95', '99997684', 'ds', 'C8E&AP', '2019-10-31 14:32:38', '2019-10-31 14:32:47', NULL, 'FLOOZ', '41357'),
(110, 2, 1, 'ed', 'DED', '99997684', 'DED', 'OOY3$&', '2019-10-31 14:47:43', '2019-10-31 14:47:49', NULL, 'FLOOZ', '41362'),
(111, 2, 1, 'de', 'DEd', '99997684', 'ded', 'J5M#VY', '2019-10-31 15:38:04', '2019-10-31 15:38:10', NULL, 'FLOOZ', '41378'),
(112, 2, 1, 'de', 'de', '99997684', 'DEe', 'USRGT@', '2019-10-31 15:55:43', '2019-10-31 15:55:48', NULL, 'FLOOZ', '41382'),
(114, 2, 1, 'QW', 'QW', '99997684', 'ES', 'XY&PT6', '2019-10-31 16:16:46', '2019-10-31 16:16:52', NULL, 'FLOOZ', '41384'),
(115, 2, 1, 'Deqws', 'De', '99997684', 'DEs', 'C5NH$5', '2019-10-31 16:28:23', '2019-10-31 16:28:28', NULL, 'FLOOZ', '41386'),
(116, 2, 1, 'deed', 'DEe', '99997684', 'DEDe', 'NV&MBJ', '2019-10-31 16:56:02', '2019-10-31 16:56:08', NULL, 'FLOOZ', '41391'),
(117, 2, 1, 'Azerty', 'Qwerty', '99997684', 'Ser', 'MSW9JF', '2019-10-31 17:18:50', '2019-10-31 17:18:58', NULL, 'FLOOZ', '41394'),
(118, 2, 1, 'Doe', 'John', '99997684', 'Lomé', 'OYZ572', '2019-10-31 18:00:04', '2019-10-31 18:00:15', NULL, 'FLOOZ', '41401'),
(119, 2, 1, 'Doe', 'John', '99997684', 'Lomé', 'K&J7W9', '2019-10-31 18:07:33', '2019-10-31 18:07:42', NULL, 'FLOOZ', '41403'),
(120, 2, 1, 'totoooo', 'Toto', '99997684', 'Dekpe', 'TIA&ZZ', '2019-11-04 17:17:43', '2019-11-04 17:17:52', NULL, 'FLOOZ', '41955'),
(121, 2, 1, 'Sodoga', 'Sara', '96341434', 'Segbe', 'O4XBG4', '2019-11-07 18:18:25', '2019-11-07 18:18:44', NULL, 'FLOOZ', '42469');

-- --------------------------------------------------------

--
-- Structure de la table `company`
--

CREATE TABLE `company` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tel` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `media` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `company`
--

INSERT INTO `company` (`id`, `name`, `tel`, `address`, `email`, `media`) VALUES
(1, 'Rakita', NULL, NULL, NULL, 'phpxYRznc13_11_2019_.png'),
(2, 'Le Cheval', NULL, NULL, NULL, 'default.jpg'),
(3, 'LK', NULL, NULL, NULL, 'default.jpg'),
(4, 'ETRAB', NULL, NULL, NULL, 'default.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `migration_versions`
--

CREATE TABLE `migration_versions` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `migration_versions`
--

INSERT INTO `migration_versions` (`version`) VALUES
('20190914110354'),
('20190916000153'),
('20190916001304'),
('20190916114818'),
('20191008153002');

-- --------------------------------------------------------

--
-- Structure de la table `travel`
--

CREATE TABLE `travel` (
  `id` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `dep` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dest` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sits` int(11) NOT NULL,
  `dep_hour` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` int(11) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `travel`
--

INSERT INTO `travel` (`id`, `company_id`, `dep`, `dest`, `sits`, `dep_hour`, `price`, `date`) VALUES
(2, 1, 'Lomé', 'Kara', 6, '14H30', 6200, '2019-09-16 00:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `logo_id` int(11) DEFAULT NULL,
  `username` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prenom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tel` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adresse` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `logo_id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `confirmation_token`, `password_requested_at`, `roles`, `nom`, `prenom`, `tel`, `adresse`) VALUES
(1, NULL, 'admin@gmail.com', 'admin@gmail.com', 'admin@gmail.com', 'admin@gmail.com', 1, 'Bq1WKm26Zph9GPmQEdzB3fyBb0P1fLELDdkgStvbhEk', 'admin{Bq1WKm26Zph9GPmQEdzB3fyBb0P1fLELDdkgStvbhEk}', '2019-11-14 11:55:17', NULL, NULL, 'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}', NULL, NULL, NULL, NULL),
(3, NULL, 'admin@icar.com', 'admin@icar.com', 'admin@icar.com', 'admin@icar.com', 1, 'xhjwtqiIdlOsg3ieGc9fF4/M0bXCr8F2jCcx2YzLE7w', 'icar@123{xhjwtqiIdlOsg3ieGc9fF4/M0bXCr8F2jCcx2YzLE7w}', '2019-11-14 11:51:54', NULL, NULL, 'a:1:{i:0;s:10:\"ROLE_ADMIN\";}', NULL, NULL, NULL, NULL);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_E00CEDDEECAB15B3` (`travel_id`);

--
-- Index pour la table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `migration_versions`
--
ALTER TABLE `migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Index pour la table `travel`
--
ALTER TABLE `travel`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_2D0B6BCE979B1AD6` (`company_id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D64992FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_8D93D649A0D96FBF` (`email_canonical`),
  ADD UNIQUE KEY `UNIQ_8D93D649C05FB297` (`confirmation_token`),
  ADD UNIQUE KEY `UNIQ_8D93D649F98F144A` (`logo_id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `booking`
--
ALTER TABLE `booking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=122;

--
-- AUTO_INCREMENT pour la table `company`
--
ALTER TABLE `company`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `travel`
--
ALTER TABLE `travel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `booking`
--
ALTER TABLE `booking`
  ADD CONSTRAINT `FK_E00CEDDEECAB15B3` FOREIGN KEY (`travel_id`) REFERENCES `travel` (`id`);

--
-- Contraintes pour la table `travel`
--
ALTER TABLE `travel`
  ADD CONSTRAINT `FK_2D0B6BCE979B1AD6` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`);

--
-- Contraintes pour la table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `FK_8D93D649F98F144A` FOREIGN KEY (`logo_id`) REFERENCES `user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

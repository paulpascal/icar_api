<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\BookingRepository")
 */
class Booking
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")

     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Travel", inversedBy="bookings")
     * @ORM\JoinColumn(nullable=false)
     */
    private $travel;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $custFirstName;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $custLastName;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $custTel;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $custAddress;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $paymentTel;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $code;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $confirmedAt;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $amountPaid;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $payment;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    private $paymentRef;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $uid;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Transaction", mappedBy="booking")
     */
    private $transactions;

    public function __construct()
    {
        $this->status = false;
        $this->createdAt = new \DateTime();
        $this->transactions = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTravel(): ?Travel
    {
        return $this->travel;
    }

    public function setTravel(?Travel $travel): self
    {
        $this->travel = $travel;

        return $this;
    }

    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(bool $status): self
    {
        $this->status = $status;
        if ($status) {
            $this->setConfirmedAt(new \DateTime());
        }
        return $this;
    }

    public function getCustFirstName(): ?string
    {
        return $this->custFirstName;
    }

    public function setCustFirstName(string $custFirstName): self
    {
        $this->custFirstName = $custFirstName;

        return $this;
    }

    public function getCustLastName(): ?string
    {
        return $this->custLastName;
    }

    public function setCustLastName(string $custLastName): self
    {
        $this->custLastName = $custLastName;

        return $this;
    }

    public function getCustTel(): ?string
    {
        return $this->custTel;
    }

    public function setCustTel(string $custTel): self
    {
        $this->custTel = $custTel;

        return $this;
    }

    public function getCustAddress(): ?string
    {
        return $this->custAddress;
    }

    public function setCustAddress(string $custAddress): self
    {
        $this->custAddress = $custAddress;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getPaymentTel(): ?string
    {
        return $this->paymentTel;
    }

    public function setPaymentTel(string $paymentTel): self
    {
        $this->paymentTel = $paymentTel;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getConfirmedAt(): ?\DateTimeInterface
    {
        return $this->confirmedAt;
    }

    public function setConfirmedAt(\DateTimeInterface $confirmedAt): self
    {
        $this->confirmedAt = $confirmedAt;

        return $this;
    }

    public function getAmountPaid(): ?int
    {
        return $this->amountPaid;
    }

    public function setAmountPaid(int $amountPaid): self
    {
        $this->amountPaid = $amountPaid;

        return $this;
    }

    public function getPayment(): ?string
    {
        return $this->payment;
    }

    public function setPayment(string $payment): self
    {
        $this->payment = $payment;

        return $this;
    }

    public function getPaymentRef(): ?string
    {
        return $this->paymentRef;
    }

    public function setPaymentRef(?string $paymentRef): self
    {
        $this->paymentRef = $paymentRef;

        return $this;
    }

    public function getUid(): ?string
    {
        return $this->uid;
    }

    public function setUid(string $uid): self
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * Generate and return a random characters string
     *
     * Useful for generating passwords or hashes.
     *
     * @return  string
     */
    function random_str()
    {
        return md5(uniqid(mt_rand()));
    }

    /**
     * @return Collection|Transaction[]
     */
    public function getTransactions(): Collection
    {
        return $this->transactions;
    }

    public function addTransaction(Transaction $transaction): self
    {
        if (!$this->transactions->contains($transaction)) {
            $this->transactions[] = $transaction;
            $transaction->setBooking($this);
        }

        return $this;
    }

    public function removeTransaction(Transaction $transaction): self
    {
        if ($this->transactions->contains($transaction)) {
            $this->transactions->removeElement($transaction);
            // set the owning side to null (unless already changed)
            if ($transaction->getBooking() === $this) {
                $transaction->setBooking(null);
            }
        }

        return $this;
    }
}

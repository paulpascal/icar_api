<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\NumericFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\RangeFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource()
 * @ApiFilter(DateFilter::class, properties={"date"})
 * @ApiFilter(NumericFilter::class, properties={"sits"}, )
 * @ApiFilter(RangeFilter::class, properties={"sits"})
 * @ApiFilter(OrderFilter::class, properties={"id", "date", "depHour"}, arguments={"orderParameterName"="order"})
 * @ORM\Entity(repositoryClass="App\Repository\TravelRepository")
 */
class Travel
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("company")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     * @Groups("company")
     */
    private $dep;

    /**
     * @ORM\Column(type="string", length=50)
     * @Groups("company")
     */
    private $dest;

    /**
     * @ORM\Column(type="integer")
     * @Groups("company")
     */
    private $sits;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     * @Groups("company")
     */
    private $depHour;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Booking", mappedBy="travel")
     */
    private $bookings;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Company", inversedBy="travels")
     */
    private $company;

    /**
     * @ORM\Column(type="integer")
     * @Groups("company")
     */
    private $price;

    /**
     * @ORM\Column(type="date")
     * @Groups("company")
     */
    private $date;

    public function __construct()
    {
        $this->bookings = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getDep(): ?string
    {
        return $this->dep;
    }

    public function setDep(string $dep): self
    {
        $this->dep = $dep;

        return $this;
    }

    public function getDest(): ?string
    {
        return $this->dest;
    }

    public function setDest(string $dest): self
    {
        $this->dest = $dest;

        return $this;
    }

    public function getSits(): ?int
    {
        return $this->sits;
    }

    public function setSits(int $sits): self
    {
        $this->sits = $sits;

        return $this;
    }

    public function getDepHour(): ?string
    {
        return $this->depHour;
    }

    public function setDepHour(string $depHour): self
    {
        $this->depHour = $depHour;

        return $this;
    }

    /**
     * @return Collection|Booking[]
     */
    public function getBookings(): Collection
    {
        return $this->bookings;
    }

    public function addBooking(Booking $booking): self
    {
        if (!$this->bookings->contains($booking)) {
            $this->bookings[] = $booking;
            $booking->setVoyage($this);
        }

        return $this;
    }

    public function removeBooking(Booking $booking): self
    {
        if ($this->bookings->contains($booking)) {
            $this->bookings->removeElement($booking);
            // set the owning side to null (unless already changed)
            if ($booking->getVoyage() === $this) {
                $booking->setVoyage(null);
            }
        }

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setDate(\DateTime $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function decrementSits($by = 1)
    {
        if ($this->sits > 0)
        $this->sits -= $by;
    }
}

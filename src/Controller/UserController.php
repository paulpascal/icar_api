<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/user")
 */
class UserController extends Controller
{
    /**
     * @Route("/", name="user_index", methods="GET")
     * @param UserRepository $userRepository
     * @return Response
     */
    public function index(UserRepository $userRepository): Response
    {
        $allAsers = $userRepository->findAll();
        $users = [];
        /** @var User $user */
        foreach ($allAsers as $user) {
            if (!$user->hasRole('ROLE_SUPER_ADMIN')) $users[] = $user;
        }
        return $this->render('user/index.html.twig', ['users' => $users]);
    }

    /**
     * @Route("/new", name="user_new", methods="GET|POST")
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->add('roles', ChoiceType::class, array(
            'multiple' => true,
            'choices' => array(
                'Administrateur' => 'ROLE_ADMIN'
                )
        ));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            try {
                $user->setEnabled(true);
                $em->persist($user);
                $em->flush();
                $this->addFlash('notification', 'Utilisateur crée avec succès!');
                return $this->redirectToRoute('user_index');
            }
            catch (UniqueConstraintViolationException $e)
            {
                $emailConstraint = $em->getRepository('App:User')->findOneBy(array(
                    'email' => $user->getEmail()
                ));

                $errors = array();
                if ($emailConstraint !== null)
                    $errors[] = 'Cette adresse email existe déjà';

                return $this->render('user/new.html.twig', [
                    'user' => $user,
                    'form' => $form->createView(),
                    'errors' => $errors
                ]);
            }
        }

        return $this->render('user/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_show", methods="GET")
     * @param User $user
     * @return Response
     */
    public function show(User $user): Response
    {
        return $this->render('user/show.html.twig', ['user' => $user]);
    }

    /**
     * @Route("/{id}/edit", name="user_edit", methods="GET|POST")
     * @param Request $request
     * @param User $user
     * @return Response
     */
    public function edit(Request $request, User $user): Response
    {
        $form = $this->createForm(UserType::class, $user);
        $form->add('enabled', CheckboxType::class);
        $form->add('roles', ChoiceType::class, array(
            'multiple' => true,
            'choices' => array(
                'Administrateur' => 'ROLE_ADMIN'
            )
        ));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            try {
                $em->flush();
                $this->addFlash('notification', 'Utilisateur modifié avec succès!');
                return $this->redirectToRoute('user_edit', ['id' => $user->getId()]);
            }
            catch (UniqueConstraintViolationException $e)
            {
                $emailConstraint = $em->getRepository('App:User')->findOneBy(array(
                    'email' => $user->getEmail()
                ));

                $errors = array();
                if ($emailConstraint !== null)
                    $errors[] = 'Cette adresse email existe déjà';

                return $this->render('user/edit.html.twig', [
                    'user' => $user,
                    'form' => $form->createView(),
                    'errors' => $errors,
                ]);
            }
        }


        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_delete", methods="DELETE")
     * @param Request $request
     * @param User $user
     * @return Response
     */
    public function delete(Request $request, User $user): Response
    {
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($user);
            $em->flush();
            $this->addFlash('notification', 'Utilisateur supprimé avec succès!');
        }

        return $this->redirectToRoute('user_index');
    }
}

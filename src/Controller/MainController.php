<?php

namespace App\Controller;

use App\Entity\User;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\MimeType\FileinfoMimeTypeGuesser;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;


class MainController extends Controller
{
    /**
     * @Route("/", name="index")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $travels = $em->getRepository('App:Travel')->count([]);
        $bookings = $em->getRepository('App:Booking')->count([]);
        $confirmedBookings = $em->getRepository('App:Booking')->count(['status' => true]);
        $waitingBookings = $em->getRepository('App:Booking')->count(['status' => false]);
        return $this->render('main/index.html.twig', array(
            'travels' => $travels, 'bookings' => $bookings,
            'confirmedBookings' => $confirmedBookings, 'waitingBookings' => $waitingBookings,
        ));
    }

    /**
     * @Route("/admin", name="admin")
     */
    public function admin()
    {


    }

    /**
     * @Route(
     *     name="api_booking_me",
     *     path="/api/v1/bookings/gencode",
     * )
     * @Method("GET")
     * @return string|null
     */
    public function generateCode()
    {
        $code = $this->get('utils')->generateBookingCode();
        return $this->json(array('code' => $code));
    }

    /**
     * @Route(
     *     name="api_booking_check",
     *     path="/api/v1/bookings/check/{id}/{uid}",
     * )
     * @Method("GET")
     * @return string|null
     */
    public function check($id, $uid)
    {
        $em = $this->getDoctrine()->getManager();
        $booking = $em->getRepository('App:Booking')->findBy(
            [
                'id' => $id, 'uid' => $uid
            ]
        );

        if ($booking) {
            return $this->json($booking);
        } else {
            return $this->json(['msg' => 'Compagnie non trouvé'], 404);
        }
    }

    /**
     * @Route(
     *     name="api_rides",
     *     path="/api/v1/rides/{id}",
     * )
     * @Method("GET")
     * @param $id
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getRide($id) {
        $em = $this->getDoctrine()->getManager();
        $company = $em->find('App:Company', $id);

        if ($company) {

        } else {
            return $this->json(['msg' => 'Compagnie non trouvé'], 404);
        }
    }

    /**
     * @Route(
     *     name="api_booking_confirm",
     *     path="/api/v1/bookings/confirm/{id}",
     * )
     * @Method("POST")
     * @param $id
     * @param Request $request
     * @return string|null
     */
    public function confirmBooking($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $booking = $em->getRepository('App:Booking')->find($id);
        if ($booking) {

            $content = $request->getContent();
            $data = json_decode($content, true);

            if (array_key_exists('paymentTel', $data) && $data['paymentTel'])
                $booking->setPaymentTel($data['paymentTel']);
            if (array_key_exists('payment_method', $data) && $data['payment_method'])
                $booking->setPayment($data['payment_method']);
            if (array_key_exists('tx_reference', $data) && $data['tx_reference'])
                $booking->setPaymentRef($data['tx_reference']);
            //  0 : Paiement réussi avec succès 2 : En cours 4 : Expiré 6: Annulé
            if (array_key_exists('status', $data) && $data['status'] == 2) {
                $booking->setStatus(true);
                $booking->setConfirmedAt(new \DateTime());

                // gen code
                $code = $this->get('utils')->random_str('alpha_up', 1);
                $code .= $this->get('utils')->random_str('alphanum2', 5);
                $booking->setCode($code);

                // update travel free places
                $booking->getTravel()->decrementSits();

                $em->flush();

                // send sms
                $res = $this->get('sms_sender')->send($booking);
                return $this->json($booking);
            }

            $em->flush();
            return $this->json($booking);
        }
        return $this->json(array('msg' => 'Cette réservation n\'existe pas ou a été déjà annulé.'), 404);
    }

    /**
     * @Route(
     *     name="api_booking_cancel",
     *     path="/api/v1/bookings/cancel/{id}",
     * )
     * @Method("GET")
     * @param $id
     * @return string|null
     */
    public function cancelBooking($id)
    {
        $em = $this->getDoctrine()->getManager();
        $booking = $em->getRepository('App:Booking')->find($id);
        if ($booking) {
            $em->remove($booking);
            $em->flush();
            return $this->json(array('msg' => 'Cette réservation a été annulé avec succès.'), 204);
        }
        return $this->json(array('msg' => 'Cette réservation n\'existe pas ou a été déjà annulé.'), 404);
    }

    /**
     * @Route(
     *     name="api_get_tg_ussd",
     *     path="/api/v1/utils/tg_ussd",
     * )
     */
    public function getTgUSSdCode()
    {
        return $this->json(array('code' => '*141*6*6*2*amount*ref#'));
    }
}

<?php

namespace App\Controller;

use App\Entity\Transaction;
use SimpleXMLElement;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Psr\Log\LoggerInterface;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Validator\Constraints\DateTime;


class TGPaymentClientController extends Controller
{
    //    const ROOT_URL = 'http://localhost:8000/';
    const ROOT_URL = 'http://icar.api.cakket.com/public';

    /**
     * @Route("/verify", defaults={"_format"="xml"}, name="tg_payment_client")
     * @param Request $request
     * @param LoggerInterface $logger
     * @return Response
     */
    public function processPayment(Request $request, LoggerInterface $logger)
    {
        $em = $this->getDoctrine()->getManager();
        $xmlContent = $request->get('data');

        if ($xmlContent) {
            $crawler = new Crawler($xmlContent);

            $data['type'] = $crawler->filterXPath('//type ')->text();
            $data['user_mobile'] = $crawler->filterXPath('//user_mobile ')->text();
            $data['user_first_name'] = $crawler->filterXPath('//user_first_name ')->text();
            $data['user_last_name'] = $crawler->filterXPath('//user_last_name ')->text();
            $data['user_lang'] = $crawler->filterXPath('//user_lang ')->text();
            $data['user_external_reference'] = $crawler->filterXPath('//user_external_reference ')->text();
            $data['billissuer_id'] = $crawler->filterXPath('//billissuer_id ')->text();
            $data['billissuer_name'] = $crawler->filterXPath('//billissuer_name ')->text();
            $data['amount'] = $crawler->filterXPath('//amount ')->text();
            $data['currency'] = $crawler->filterXPath('//currency ')->text();
            $data['bill_ref'] = $crawler->filterXPath('//bill_ref ')->text();

            if ($crawler->filterXPath('//option ')->count())
                $data['option'] = $crawler->filterXPath('//option ')->text();
            else
                $data['option'] = null;

            $data['pay_mode'] = $crawler->filterXPath('//pay_mode ')->text();
            $logger->info('##### DATA #####', ['method' => 'TMONEY']);
            $logger->info(print_r($data, true), ['method' => 'TMONEY']);
            $logger->info('##### /DATA #####', ['method' => 'TMONEY']);

            $xmlResponse = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8" ?><BillIssuerAPI></BillIssuerAPI>');
            if ($data['type'] === 'PREAUTH') {

                if ($data['option']) {
                    // get list unpaid bills by bill ref
                    $booking = $em->getRepository('App:Booking')->findOneBy(
                        array('uid' => $data['bill_ref'])
                    );
                    // validating user choice
                    $xmlResponse->addChild('status', 'OK');
                    $xmlResponse->addChild('info', ' Taga / ' . $booking->getTravel()->getDep() .
                        ' - ' . $booking->getTravel()->getDest() .
                        ' [' . $booking->getTravel()->getDate()->format('d-m-Y') . ']');
                    $xmlResponse->addChild('amount', $booking->getTravel()->getPrice());
                } else if ($data['bill_ref']) {
                    // get list unpaid bills by bill ref
                    $booking = $em->getRepository('App:Booking')->findOneBy(
                        array('uid' => $data['bill_ref'])
                    );
                    if ($booking) {
                        $xmlResponse->addChild('status', 'OK');
                        $select = $xmlResponse->addChild('select');
                        $option = $select->addChild('option');
                        $option->addAttribute('amount', $booking->getTravel()->getPrice());
                        $option->addAttribute('id', $booking->getId());
                        $option->addAttribute('label', ' Taga / ' . $booking->getTravel()->getDep() .
                            ' - ' . $booking->getTravel()->getDest() .
                            ' [' . $booking->getTravel()->getDate()->format('d-m-Y') . ']');
                    } else {
                        $xmlResponse->addChild('status', 'KO');
                        $xmlResponse->addChild('errormessage', 'Code référence invalide.');
                    }
                } else {
                    $xmlResponse->addChild('status', 'KO');
                    $xmlResponse->addChild('errormessage', 'Code référence requis.');
                }
            } else if ($data['type'] === 'TRANSACTION') {

                // get bill_ref
                $billRef = $data['bill_ref'];
                if ($billRef) {
                    // bill ref here will be the booking ui id
                    $booking = $em->getRepository('App:Booking')->findOneBy(
                        array('uid' => $billRef)
                    );
                    if ($booking) {
                        $booking->setCode($this->get('utils')->generateBookingCode());
                        $booking->setPayment('TMONEY');
                        $booking->setAmountPaid($data['amount']);

                        $transaction = new Transaction();
                        $transaction->setBooking($booking);
                        $transaction->setMethod('TMONEY');
                        $transaction->setNumber($data['user_mobile']);
                        $em->persist($transaction);
                        $em->flush();

                        $xmlResponse->addChild('status', 'OK');
                        $xmlResponse->addChild('transactionid', $transaction->getId());
                        $xmlResponse->addChild('vouchercode', htmlspecialchars($booking->getCode()));
                        $xmlResponse->addChild('amount', $booking->getTravel()->getPrice());
                        $xmlResponse->addChild('callback', $this::ROOT_URL . '/booking_pay_ref/' . $booking->getId());
                    } else {
                        $xmlResponse->addChild('status', 'KO');
                        $xmlResponse->addChild('errormessage', 'Code de référence invalide.');
                    }
                } else {
                    $xmlResponse->addChild('status', 'KO');
                    $xmlResponse->addChild('errormessage', 'Code de référence requis.');
                }
            }

            $response = new Response($xmlResponse->asXML());
            $response->headers->set('Content-Type', 'xml');

            $logger->info('##### RESPONSE #####', ['method' => 'TMONEY']);
            $logger->info($response->getContent(), ['method' => 'TMONEY']);
            $logger->info('##### /RESPONSE #####', ['method' => 'TMONEY']);

            return $response;
        }
        else {
            $xmlResponse = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8" ?><BillIssuerAPI></BillIssuerAPI>');
            $xmlResponse->addChild('status', 'KO');
            $xmlResponse->addChild('errormessage', 'Aucune donnée reçu par Taga');
            $response = new Response($xmlResponse->asXML());
            return  $response;
        }
    }

    /**
     * @Route("/booking_pay_ref/{id}", name="tg_transaction_ref")
     * @param $id
     * @param Request $request
     * @param LoggerInterface $logger
     * @return Response
     */
    public function getTransactionId($id, Request $request, LoggerInterface $logger) {
        $tagpay_ref= $request->query->get('tagpay_ref');
        $em = $this->getDoctrine()->getManager();
        $booking = $em->find('App:Booking', $id);
        if ($booking) {
            $booking->setPaymentRef($tagpay_ref);
            $booking->setConfirmedAt(new \DateTime());
            $booking->setStatus(true);
            $booking->getTravel()->decrementSits();
            $em->flush();
        }

        $xmlResponse = new SimpleXMLElement('<BillIssuerAPI></BillIssuerAPI>');
        $xmlResponse->addChild('booking_id', $booking->getId());
        $xmlResponse->addChild('tagpay_ref', $tagpay_ref);
        $response = new Response($xmlResponse->asXML());
        $response->headers->set('Content-Type', 'xml');
        $logger->info('##### PAY_REF #####', ['method' => 'TMONEY']);
        $logger->info($response->getContent(), ['method' => 'TMONEY']);
        $logger->info('##### /REF #####', ['method' => 'TMONEY']);

        return new Response();
    }
}

<?php

namespace App\Form;

use App\Entity\Booking;
use App\Entity\Travel;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BookingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('status')
            ->add('custFirstName')
            ->add('custLastName')
            ->add('custTel')
            ->add('custAddress')
            ->add('code')
            ->add('createdAt')
            ->add('confirmedAt')
            ->add('amountPaid')
            ->add('payment')
            ->add('paymentRef')
            ->add('travel', EntityType::class, array(
                'class' => Travel::class,
                'choice_label' => function(Travel $t) {
                    return $t->getCompany()->getName() . ' - ' . $t->getDep() . ' --> ' .
                           $t->getDest() .  ' - ' . $t->getDate()->format('d/m/yyyy');
                }
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Booking::class,
        ]);
    }
}

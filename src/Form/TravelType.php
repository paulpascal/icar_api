<?php

namespace App\Form;

use App\Entity\Company;
use App\Entity\Travel;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TravelType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dep', TextType::class, array(
                'label' => 'Lieux de départ',
                'data' => 'Lomé',
                'empty_data' => 'Lomé',
                'attr' => array('disabled' => 'disabled')
            ))
            ->add('dest', TextType::class, array(
                'label' => 'Lieux d\'arrivé',
                'data' => 'Kara',
                'empty_data' => 'Kara',
                'attr' => array('disabled' => 'disabled')
            ))
            ->add('price', IntegerType::class, array(
                'label' => 'Montant'
            ))
            ->add('sits', IntegerType::class, array(
                'label' => 'Nombre de places disponible',
                'attr' => array('min' => 1)
            ))
            ->add('date', DateType::class, array(
                'label' => 'Date du voyage',
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'required' => true
            ))
            ->add('depHour', TextType::class, array(
                'label' => 'Heure de départ',
                'required' => false,
                'attr' => array('placeholder' => 'Optionnel')
            ))
            ->add('company', EntityType::class, array(
                'label' => 'Compagnie',
                'class' => Company::class,
                'choice_label' => 'name'
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Travel::class,
        ]);
    }
}

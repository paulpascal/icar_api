<?php

namespace App\EventListener;

use App\Entity\Booking;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;

class BookingListener
{
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if (!$entity instanceof Booking) {
            return;
        }

        $entityManager = $args->getObjectManager();
        $uidSet = false;

        do {
            $uid = mt_rand(1, 9999999999);
            $hasOccurence = $entityManager->getRepository('App:Booking')->count(
                array(
                    'uid' => $uid
                )
            );
            if ($hasOccurence == 0) {
                $entity->setUid($uid);
                $entityManager->flush();
                $uidSet= true;
            }
        } while (!$uidSet);
    }
}
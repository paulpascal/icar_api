<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190914110354 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE company (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) NOT NULL, tel VARCHAR(20) NOT NULL, address VARCHAR(255) NOT NULL, email VARCHAR(50) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE booking (id INT AUTO_INCREMENT NOT NULL, voyage_id INT NOT NULL, status TINYINT(1) NOT NULL, cust_first_name VARCHAR(100) NOT NULL, cust_last_name VARCHAR(100) NOT NULL, cust_tel VARCHAR(20) NOT NULL, cust_address VARCHAR(255) NOT NULL, code VARCHAR(10) NOT NULL, created_at DATETIME NOT NULL, confirmed_at DATETIME NOT NULL, amount_paid INT NOT NULL, payment VARCHAR(50) NOT NULL, INDEX IDX_E00CEDDE68C9E5AF (voyage_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE travel (id INT AUTO_INCREMENT NOT NULL, company_id INT DEFAULT NULL, dep VARCHAR(50) NOT NULL, dest VARCHAR(50) NOT NULL, sits INT NOT NULL, dep_hour VARCHAR(10) NOT NULL, INDEX IDX_2D0B6BCE979B1AD6 (company_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE booking ADD CONSTRAINT FK_E00CEDDE68C9E5AF FOREIGN KEY (voyage_id) REFERENCES travel (id)');
        $this->addSql('ALTER TABLE travel ADD CONSTRAINT FK_2D0B6BCE979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE travel DROP FOREIGN KEY FK_2D0B6BCE979B1AD6');
        $this->addSql('ALTER TABLE booking DROP FOREIGN KEY FK_E00CEDDE68C9E5AF');
        $this->addSql('DROP TABLE company');
        $this->addSql('DROP TABLE booking');
        $this->addSql('DROP TABLE travel');
    }
}

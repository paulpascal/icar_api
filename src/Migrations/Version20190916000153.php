<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190916000153 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE booking CHANGE status status TINYINT(1) DEFAULT NULL, CHANGE code code VARCHAR(10) DEFAULT NULL, CHANGE created_at created_at DATETIME DEFAULT NULL, CHANGE confirmed_at confirmed_at DATETIME DEFAULT NULL, CHANGE amount_paid amount_paid INT DEFAULT NULL, CHANGE payment payment VARCHAR(50) DEFAULT NULL');
        $this->addSql('ALTER TABLE company ADD media VARCHAR(255) DEFAULT NULL, CHANGE tel tel VARCHAR(20) DEFAULT NULL, CHANGE address address VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE travel CHANGE dep_hour dep_hour VARCHAR(10) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE booking CHANGE status status TINYINT(1) NOT NULL, CHANGE code code VARCHAR(10) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE created_at created_at DATETIME NOT NULL, CHANGE confirmed_at confirmed_at DATETIME NOT NULL, CHANGE amount_paid amount_paid INT NOT NULL, CHANGE payment payment VARCHAR(50) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE company DROP media, CHANGE tel tel VARCHAR(20) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE address address VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE travel CHANGE dep_hour dep_hour VARCHAR(10) NOT NULL COLLATE utf8mb4_unicode_ci');
    }
}

<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190916001304 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE booking DROP FOREIGN KEY FK_E00CEDDE68C9E5AF');
        $this->addSql('DROP INDEX IDX_E00CEDDE68C9E5AF ON booking');
        $this->addSql('ALTER TABLE booking CHANGE voyage_id travel_id INT NOT NULL');
        $this->addSql('ALTER TABLE booking ADD CONSTRAINT FK_E00CEDDEECAB15B3 FOREIGN KEY (travel_id) REFERENCES travel (id)');
        $this->addSql('CREATE INDEX IDX_E00CEDDEECAB15B3 ON booking (travel_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE booking DROP FOREIGN KEY FK_E00CEDDEECAB15B3');
        $this->addSql('DROP INDEX IDX_E00CEDDEECAB15B3 ON booking');
        $this->addSql('ALTER TABLE booking CHANGE travel_id voyage_id INT NOT NULL');
        $this->addSql('ALTER TABLE booking ADD CONSTRAINT FK_E00CEDDE68C9E5AF FOREIGN KEY (voyage_id) REFERENCES travel (id)');
        $this->addSql('CREATE INDEX IDX_E00CEDDE68C9E5AF ON booking (voyage_id)');
    }
}

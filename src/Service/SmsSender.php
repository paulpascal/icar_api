<?php
/**
 * Created by PhpStorm.
 * User: paulpascal
 * Date: 11/1/18
 * Time: 1:19 AM
 */

namespace App\Service;


use App\Entity\Booking;
use MessageBird\Client;
use MessageBird\Exceptions\HttpException;
use MessageBird\Exceptions\RequestException;
use MessageBird\Exceptions\ServerException;
use MessageBird\Objects\Message;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SmsSender
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function send(Booking $booking)
    {
//        $messageBird = new Client('OBl8k0fUCiajJqvyWFEQd2a0E'); paul
        $messageBird = new Client('HDdXf052x1ZiIapS1wsfbtgM5'); // kev
        $message = new Message();
        $message->originator = 'Taga';
        $message->recipients = [ '+228'.$booking->getCustTel() ];


        $message->body = 'Cher(e) ' . $booking->getCustFirstName() . ' ' . $booking->getCustLastName() .
                         ' , le paiement de ' . $booking->getTravel()->getPrice() . ' pour votre réservation de voyage chez ' . $booking->getTravel()->getCompany()->getName() .
                         ' : ' . $booking->getTravel()->getDep() . ' - ' . $booking->getTravel()->getDest() .
                         ' a été effectué avec succès. Votre code est: ' . $booking->getCode() . '.
Enfin Taga vous souhaite un bon voyage.';

        try {
            $response = $messageBird->messages->create($message);
            if ($response->recipients->totalCount == $response->recipients->totalSentCount) {
                return true;
            } else {
                return false;
            }
        } catch (HttpException $e) {
            return false;
        } catch (RequestException $e) {
            return false;
        } catch (ServerException $e) {
            return false;
        }
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: paulpascal
 * Date: 11/1/18
 * Time: 1:19 AM
 */

namespace App\Service;


use App\Entity\Booking;
use MessageBird\Client;
use MessageBird\Exceptions\HttpException;
use MessageBird\Exceptions\RequestException;
use MessageBird\Exceptions\ServerException;
use MessageBird\Objects\Message;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Utils
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function generateBookingCode()
    {
        $code = $this->random_str('alpha_up', 1);
        $code .= $this->random_str('alphanum2', 5);
        return $code;
    }

    /**
     * Generate and return a random characters string
     *
     * Useful for generating passwords or hashes.
     *
     * The default string returned is 8 alphanumeric characters string.
     *
     * The type of string returned can be changed with the "type" parameter.
     * Seven types are - by default - available: basic, alpha, alphanum, num, nozero, unique and md5.
     *
     * @param   string  $type    Type of random string.  basic, alpha, alphanum, num, nozero, unique and md5.
     * @param   integer $length  Length of the string to be generated, Default: 8 characters long.
     * @return  string
     */
    function random_str($type = 'alphanum2', $length = 8)
    {
        switch($type)
        {
            case 'basic'    : return mt_rand();
                break;
            case 'alpha'    :
            case 'alpha_up'    :
            case 'alphanum' :
            case 'alphanum2' :
            case 'num'      :
            case 'nozero'   :
                $seedings             = array();
                $seedings['alpha']    = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $seedings['alpha_up'] = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $seedings['alphanum'] = '#@$0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $seedings['alphanum2'] = '#@01234ABCDEFGPQRSTUVWXYZ56789HIJKLMNO$';
                $seedings['num']      = '0123456789';
                $seedings['nozero']   = '123456789';

                $pool = $seedings[$type];

                $str = '';
                for ($i=0; $i < $length; $i++)
                {
                    $str .= substr($pool, mt_rand(0, strlen($pool) -1), 1);
                }
                return $str;
                break;
            case 'unique'   :
            case 'md5'      :
                return md5(uniqid(mt_rand()));
                break;
        }
    }
}
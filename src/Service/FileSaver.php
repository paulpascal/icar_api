<?php
/**
 * Created by PhpStorm.
 * User: paulpascal
 * Date: 11/1/18
 * Time: 1:19 AM
 */

namespace App\Service;


use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\File;

class FileSaver
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function save(File $file)
    {
        // Generate a unique name for the file before saving it
        $fileName =  $file->getFilename() . (new \DateTime())->format('d_m_Y_') . '.' . $file->guessExtension();

        // Move the file to the directory where brochures are stored
        $file->move(
            $this->container->getParameter('uploads_dir'), $fileName
        );

        return $fileName;
    }
}